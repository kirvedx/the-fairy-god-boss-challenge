/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import del from 'del';                              // Used by our clean process, for the most part -
import bump from 'gulp-bump-version';               // Used for version increment automation
import runsequence from 'run-sequence';             // For running tasks sequentiallys


// GLOBALS

const args =                                        // Allows us to accept arguments to our gulpfile
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption )
            {
                // argument value
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check that we supplied any commands sepecific to the gulp-bump-version gulp plug-in:
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly:
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = { version: toVersion, key: '"version": "' };
}

// If type was provided, overwrite the options accordingly:
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = { type: byType, key: '"version": "' };
}


// Bump the version in our projects files:
gulp.task
(
    'bump-file-versions',
    () =>
    {
        // Start with all the files using the typical key, then hit our package.json
        // 'dist/src/**/*.js':
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.tsx',
                '**/*.js',
                '**/*.scss',
                '!node_modules{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


// Bump the version in our package.json file:
gulp.task
(
    'bump-project-version',
    () =>
    {
        // Target our package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:dist',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning build result...', ' ' );

        let deleteList = ( !args.alt ) ? 'dist' : 'dist';
        return del.sync
        (
            deleteList,
            { force: true }
        );
    }
);


// Cleans the dist directory and bump all file header version strings:
gulp.task
(
    'build-release',
    callback =>
    {
        runsequence
        (
            'clean:dist',
            'bump-project-version',
            'bump-file-versions',
            callback
        );
    }
);
