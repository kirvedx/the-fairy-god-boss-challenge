# the-fairy-god-boss-challenge

[![pipeline status](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/badges/master/pipeline.svg)](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/commits/master)  [![coverage report](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/badges/master/coverage.svg)](https://devrikx.gitlab.io/the-fairy-god-boss-challenge/coverage/)

A React.js application which satisifies the requirements of the coding challenge I, Richard B Winters, have been asked to complete.

## TOC
* [Objective](#objective)
* [Requirements](#requirements)
  * [Bonus Asks](#bonus-asks)
  * [In Review](#in-review)
  * [Submission](#submission)
* [Implementation Details](#implementation-details)
  * [Additional Considerations](#additional-considerations)
* [Instructions for Review](#instructions-for-review)
  * [Git the Source](#git-the-source)
  * [Install Dependencies](#install-dependencies)
  * [Build the Application](#build-the-application)
  * [Test the application](#test-the-application)
* [Other Reviewables](#other-reviewables)
  * [Gitflow](#gitflow)
  * [Continuous Integration and Deployment](#continuous-integration-and-deployment)
  * [FGB: A React Challenge](#fgb-a-react-challenge)
* [Tools](#tools)

## Objective

The goal of this test is to create a front end for a very simple messaging board using React.js as the primary tool for building the user interface. This board should act as a responsive single page application that contains 3 pages:

* The **landing/view posts** page where you can *see all posts*.
* The **create a post** page that allows you to *create a post*.
* The **post details** page that will allow you to *read a post, its replies, and add your own reply*.

## Requirements

* You have as much time as you want/need to complete this. The expectation is it will take a few hours.
* You must use React.js as the primary view layer.
* You **do not** need to persist the message board's messages to a backend - you can just:
  * Store the data in React's state and have it wipe on refresh.
  * Use something like redux as a store and have it wipe on refresh.
  * Use localstorage if you want persistance.
  * Etc.
* Each page has a mobile and web breakpoint/wireframe. The result must be responsive.
  * *It is recommended you use a grid system like bootstrap.*
* You may use any tools/libraries that you want, such as:
  * React-router, Redux, etc.
  * SASS, less, etc.
  * Webpack, Gulp, Grunt, Yarn, NPM, etc.
  * Bootstrap Components, etc.
  * A React/Webpack starter kit
* You, however, **cannot** use pre-created message board components or pages, and main components in the message board should be your own.
* The result does not have to be fully styled but it should at least have constant spacing, padding, margins, alignment, etc.
* Not *required*, but we recommend you use ES6 JavaScript.
* **Please notice the subtle differences between the mobile vs desktop breakpoints***.

### Bonus Asks

* Add client side form validation and error handling
* Add some unit tests to your code
* Add some style/flair of your own as long as it keeps true to the ideas presented in the wireframes

### In Review

The following is expected and/or sought from the exercise:

* That the test works.
* That you are proficient in React.js.
* That you understand and can implement responsive design.
* How you structure your implementation.
* What tools you decided to use and how you leveraged them.

 Keep it simple! A Simple, well executed test will look better than something fancy that does not quite work.

### Submission

Please [e-mail](mailto:?) the code for the test or invite [user's email removed] to your private git repository with instructions on how to get it working. (This repository is now public so as to provide an example of my ability with React, as well as advanced project management features available in services such as Gitlab.)

## Implementation Details

The application shell consists of the following:

* NPM for project dependency management
* Gulp/Webpack for source compilation/optimization
* Bootstrap/Material Design for complimenting a standard in design
* React.js as the primary view layer
  * Redux may be leveraged as a store for internal state management, but it is not required.

### Additional Considerations

In consideration of the following:

> It is recommended that ES6 syntax should be leveraged in developing the application. Data persistence is not necessary.

I am using Typescript - which makes use of ES6 syntax, but which additionally provides a typing system. While it is not a requirement for the challenge, I hoped to show the benefits of using Typescript, highlight my ability in doing so - while also satisfying the recommendation of using ES6 syntax and subsequently prove my ability in that regard.

## Instructions for Review

The following steps should be taken in order to review the challenge submission:

### Git the Source

Clone the repository from [Gitlab](https://gitlab.com/devrikx/the-fairy-god-boss-challenge):

```bash
git clone https://git@gitlab.com:devrikx/the-fairy-god-boss-challenge
```

Keep in mind that this is a private repository, you will be asked for your username and password through the console. You should have received an invitation to create a user account at the e-mail that was provided in the code challenge document.

### Install dependencies

The following dependencies are required:

* Node.js (latest version is ideal)
  * NPM, which will come pre-bundled with Node.js.

You may use Yarn if you like, however, NPM will suffice.

Head back to your terminal, and proceed to install the additional dependencies as follows:

```bash
cd the-fairy-god-boss-challenge
npm install -g webpack
npm install .
```

### Build the Application

Now that you have all the dependencies installed, run the following command to build the application:

```bash
npm run build
```

That's it! You've successfully prepared the application for review.

### Test the Application

There are two different ways you can test the application:

#### Development

To test the application as a developer, you may leverage the tests which are provided with the source. The tests are built using [Jest](https://jestjs.io/docs/en/tutorial-react) and [ts-jest](https://github.com/kulshekhar/ts-jest/).

Run the following command:

```bash
npm test
```

Snapshots are provided with the source for the the Jest environment to test against. If you contribute to the code base, make sure that you make any adustments to the tests that are necessary, though, you should rarely need to modify the existing tests. If there *are* any changes to the snapshot(s), you will need to update them and ensure that they are included in your commits. You can do this by either:

* Deleting the `__snapshots__` directories for any components whose snapshots have been modified.
* Running the `jest --updateSnapshot` command to re-record every snapshot that fails.
  * Please note that you'll need to either script this through NPM, or install jest-cli.

#### Production

Open `public/index.html` in your favorite browser (*Chrome recommended*) to test the application.

## Other Reviewables

As a compliment to the code itself, I also made use of some common features of Gitlab with hopes of displaying my comfort in performing common project management tasks, as well as in making use of best practices.

I have leveraged the Project Management features of Gitlab in the following ways:

### Gitflow

* The [project board](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/boards?=) was leveraged in creating [user-sceanrios/tasks](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/issues), and [milestones](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/milestones) for guiding the completion of the code challenge.
* Issue-First Development was leveraged in completing this code challenge:
  * [Commits](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/commits/master) were made frequently, and only when complete changes were made.
  * [Feature branches](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/network/master) were leveraged in creating the form validation and error handling features.
  * [Task branches](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/network/master) were leveraged in the burn-down of the form validation and error handling features.
  * Notice there is a tag for the single release of the application, and a related 1-0-stable branch where the release could potentially be maintained as development continues on the master branch.

### Continuous Integration and Deployment

I have leveraged the job/pipeline features of Gitlab in the following ways:

* When checking in the master branch, a [pipline](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/pipelines) is utilized in testing, building, and deploying the challenge application.
  * Two [jobs](https://gitlab.com/devrikx/the-fairy-god-boss-challenge/-/jobs) are dispatched with every commit to master; Test and Deploy.

### FGB: A React Challenge

You can review the [FGB: React Challenge Application](https://devrikx.gitlab.io/the-fairy-god-boss-challenge/), continuously deployed to Gitlab pages upon a check-in to master.

**NOTE**

Under normal circumstances, while utilizing Gitlab Git flow, you would continuously deploy from a relase branch - though this requires additional configuration that I felt would be excessive and unneccessary for the purposes of this challenge. I only intended to prove my ability, and hint at the extent of my experience, by providing a configuration even this involved.

The usage of agile development and best practices added only a minimal subset of additional activity in completing the code challenge, and helped to keep the project organized and extremely easy to review. To minimize the time spent on completing the challenge, I only utilized the branching policies of the Gitlab Git flow for the form validation and error handling features.

## Tools

The tools I leveraged in competing this code challenge include:

* [Visual Studio Code](https://code.visualstudio.com/)