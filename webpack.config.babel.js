 /*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // INCLUDES
 import * as path from 'path';
 import HtmlWebPackPlugin from "html-webpack-plugin";
 import MiniCssExtractPlugin from "mini-css-extract-plugin";


// DEFINES


module.exports =
{
    entry:
    {   // We specify the entry so we can use a custom entry point
        // Otherwise index.js is expected (anywhere in folder stucture)
        app: "./src/app.tsx"
    },

    output:
    {
        //filename: "bundle.js",
        path: path.resolve(__dirname, 'dist'),
    },

    // Enable sourcemaps for debugging webpack's output:
    devtool: "source-map",

    resolve:
    {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ['.ts', '.tsx', '.js', '.jsx', ".css", ".scss", '.json']
    },

    module:
    {
        rules:
        [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader':
            {
                test: /\.tsx?$/,
                use:
                [
                    {
                        loader: "awesome-typescript-loader"
                    }
                ]
            },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader':
            {
                enforce: "pre",
                test: /\.js$/,
                use:
                [
                    {
                        loader: "source-map-loader"
                    }
                ]
            },

            // ES6/7 JavaScript will be handled by babel-loader
            {
                test: /\.js$/,
                //exclude: /node_modules/,
                use:
                [
                    {
                        loader: "babel-loader"
                    }
                ]
            },

            {   // HTML Template
                test: /\.html$/,
                use: [{ loader: "html-loader", options: { minimize: false } }]
            },

            {   // Image Optimization
                test: /\.(gif|png|jpe?g|svg)$/i,
                use:
                [
                    {
                        loader: "url-loader",
                        options:
                        {
                            exclude: /\.(svg)$/i,
                            name: "./images/[name]_[hash:7].[ext]",
                            limit: 10000
                        }
                    },
                    {
                        loader: 'img-loader'
                    }
                ]
            },

            {   // SCSS Compilation
                test: /\.scss$/,
                //include: [
                //    path.resolve(__dirname, "src/scss")
                //],
                use:
                [
                    {
                        loader: "style-loader"
                    },
                    {
                        // We'll use a drop-in replacement for css-loader which supports importing
                        // SCSS within a typescript file
                        //loader: "css-loader"
                        loader: "typings-for-css-modules-loader",
                        options:
                        {
                            modules: true,
                            namedExport: true,
                            camelCase: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options:
                        {
                            includePaths: ["node_modules"]
                        }
                    }
                ]
            }
        ]
    },
    plugins:
    [
        new HtmlWebPackPlugin
        (
            {
                template: "src/index.html",
                filename: "./index.html"
            }
        ),
        new MiniCssExtractPlugin
        (
            {
                filename: "[name].css",
                chunkFilename: "[id].css"
            }
        )
    ],

    // When importing a module whose path matches one of the following, just assume a corresponding global
    // variable exists and use that instead. This is important as it allows us to avoid bundling all of our
    // dependencies, which subsequently allows browsers to cache those libraries between builds:
    //externals:
    //{
    //    "react": "React",
    //    "react-dom": "ReactDOM"
    //}
};