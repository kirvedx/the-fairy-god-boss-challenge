/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { ADD_POST } from "../constants/action-types";
import { UPDATE_POST } from "../constants/action-types";


// DEFINES



// ACTIONS

/* An action for adding posts: */
export const addPost = ( post: any ) => ( { type: ADD_POST, payload: post } );

/* An action for updating the comments within in each post: */
export const updatePost = ( comment: any ) => ( { type: UPDATE_POST, payload: comment } );
