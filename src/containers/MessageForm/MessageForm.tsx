/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { addPost } from "../../redux/actions";
import { TimestampHelper, FormValidation } from "../../helpers";
import { MessageItemType, InputValidationType } from "../../library";
import { CreateMessageForm } from "../../components/CreateMessageForm";


// DEFINES
interface DispatchFromProps {
    addPost: ( post: any ) => any
}

const mapDispatchToProps = ( dispatch: any ) =>
{
    return {
      addPost: ( post: any ) => dispatch( addPost( post ) )
    };
};


/**
 * Message form container component
 *
 * @since 0.1.0
 */
export class MessageFormContainer extends React.Component<MessageItemType, any>
{
    /**
     * @var { string } displayName Always set the display name.
     *
     * @since 0.1.0
     */
    displayName = "MessageFormContainer";


    /**
     * We'd set propTypes here if this wasn't Typescript.
     * We'd set default  props here if we wanted them.
     */


    /**
     * Class constructor
     *
     * @param props
     */
    constructor( props?: any )
    {
        // Always call super()!
        super( props );

        this.state =
        {
            title: "",
            author: "",
            content: "",
            excerpt: "",
            created: "",
            updated: "",
            comments: [],
            formValid: false,
            validations: {
                title: {
                    valid: false,
                    exists: false,
                    required: true,
                    label: "",
                    input: "",
                    validation: {
                        class: "",
                        message: ""
                    }
                },
                content: {
                    valid: false,
                    exists: false,
                    required: true,
                    label: "",
                    input: "",
                    validation: {
                        class: "",
                        message: ""
                    }
                },
                author: {
                    valid: false,
                    exists: false,
                    required: true,
                    label: "",
                    input: "",
                    validation: {
                        class: "",
                        message: ""
                    }
                }
            }
        };

        // Here we bind our handlers to an instance of 'this':
        this.handleTitleChange = this.handleTitleChange.bind( this );
        this.handlePostContentChange = this.handlePostContentChange.bind( this );
        this.handleAuthorChange = this.handleAuthorChange.bind( this );
        //this.handleInput = this.handleInput.bind( this );
        this.handleSubmit = this.handleSubmit.bind( this );
        //this.setFormValidation = this.setFormValidation.bind( this );
    }


    /**
     * Makes use of a validation helper for implementing form validation
     *
     * @param { string } field The field validation is run for
     * @param { string } value A fields input, converted to a string
     */
    getValidation( field: string, value: string )
    {
        // This method uses our validation helper for implementing form validation
        let options = {
            required: false,
            constrain: false,
            exclude: false,
            regex: /[~`!@#$%\^&*()_+=\-\[\]\\';,/{}|\\":<>\?]/g,
            errorMessage:""
        },
        validator = new FormValidation(),
        formField: InputValidationType;

        switch( field )
        {
            case 'title':
            {
                // We have some helper functions to simplify required code
                // when implementing form field validation:
                options.required = this.state.validations.title.required;
                options.constrain = true;
                options.exclude = true;
                options.regex = /[~`$%\^*+=\[\]\;/{}|\\"<>]/g;
                options.errorMessage = "The title may only contain alphanumeric and the following special characters: [!,?,:, @, #, &, (,), -]";

                formField = validator.stageValidation( value, options );
            }break;

            case 'content':
            {
                // We have some helper functions to simplify required code
                // when implementing form field validation:
                options.required = this.state.validations.content.required;
                options.constrain = false;
                options.exclude = false;

                formField = validator.stageValidation( value, options );
            }break;

            case 'author':
            {
                // We have some helper functions to simplify required code
                // when implementing form field validation:
                options.required = this.state.validations.author.required;
                options.constrain = true;
                options.exclude = true;
                options.regex = /[~`$%\^*+=\[\]\;/{}|\\"<>]/g;
                options.errorMessage = "The title may only contain alphanumeric and the following special characters: [!,?,:, @, #, &, (,), -]";

                formField = validator.stageValidation( value, options );
            }break;
        }

        return formField;
    }


    /**
     * Handles state and validation for the title form field
     *
     * @param { Event } event
     *
     * @return { void }
     */
    handleTitleChange( event: any )
    {
        // Get the validation state for this input:
        let titleValidation = this.getValidation( event.target.id, event.target.value ),
            { validations, formValid } = this.state;

        // Update the state:
        validations = { ...validations, title: titleValidation  };
        formValid = ( validations.title.valid && validations.content.valid && validations.author.valid );

        // Set the title by the input provided:
        this.setState( { [event.target.id]: event.target.value } );
        this.setState( { validations, formValid } );
    }


    /**
     * Handles state and validation for the message content form field
     *
     * @param { Event } event
     *
     * @return { void }
     */
    handlePostContentChange( event: any )
    {
        // Get the validation state for this input:
        let contentValidation = this.getValidation( event.target.id, event.target.value ),
            { validations, formValid } = this.state;

        // Update the state:
        validations = { ...validations, content: contentValidation  };
        formValid = ( validations.title.valid && validations.content.valid && validations.author.valid );

        // Set the post contents by the input provided:
        this.setState( { [event.target.id]: event.target.value } );
        this.setState( { validations, formValid } );
    }


    /**
     * Handles state and validation for the author form field
     *
     * @param { Event } event
     *
     * @return { void }
     */
    handleAuthorChange( event: any )
    {
        // Get the validation state for this input:
        let authorValidation = this.getValidation( event.target.id, event.target.value ),
            { validations, formValid } = this.state;

        // Update the state:
        validations = { ...validations, author: authorValidation  };
        formValid = ( validations.title.valid && validations.content.valid && validations.author.valid );

        // Set the author by the input provided:
        this.setState( { [event.target.id]: event.target.value } );
        this.setState( { validations, formValid } );
    }


    /**
     * Handles procedures required upon submission of our create a new post form
     *
     * @param { Event } event
     *
     * @return { void }
     */
    handleSubmit( event: any )
    {
        event.preventDefault();

        // Get a timestamp ready, by preparing the seconds since epoch. We'll use
        // the number as the id, its localeString() as the created timestamp, as
        // well as the intial updated timestamp:
        let d = new Date();
        let seconds = Math.round(d.getTime() / 1000);

        // Create the id:
        const id = seconds;

        // Set the created and updated timestamps:
        let stamper = new TimestampHelper();
        const created = stamper.getTimestamp( ( seconds * 1000 ) );
        const updated = created;

        // Pull the user-contributed values:
        let { title, content, author, comments } = this.state;

        // Trim whitespace characters:
        title = title.trim();
        content = content.trim();
        author = author.trim();

        // If no title was specified, do not submit and display an error to the user:
        ( !title || title.trim() === "" )? "" : "";

        // If no content was specified, don't submit and display an error to the user:
        ( !content || content.trim() === "" )? "" : "";

        // If no author was specified, do not submit and display an error to the user:
        ( !title || title.trim() === "" )? "" : "";

        // Create an excerpt:of the full content of the post:
        const excerpt = content.substring( 0, 100 );

        // Make the POST to our Redux store:
        ( this.props as any ).addPost( { id, title, content, author, excerpt, created, updated, comments } );

        // Redirect to the message-board:
        ( this.props as any ).history.push( '/view-post/' + id );

        // Clear the state of this component for a subsequent run-through:
        this.setState( { title: "", content: "", author: "",  excerpt: "", created: "", updated: "", comments: [] } );
    }


    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 0.1.0
     */
    render()
    {
        // Move our state properties to a variable for cleaner manipulation:
        const { title, content, author, validations, formValid } = this.state;


        return (
            <CreateMessageForm
              title={title}
              titleValidation={validations.title}
              content={content}
              contentValidation={validations.content}
              author={author}
              authorValidation={validations.author}
              submitState={!formValid}
              handleTitleChange={this.handleTitleChange}
              handleAuthorChange={this.handleAuthorChange}
              handlePostContentChange={this.handlePostContentChange}
              handleSubmit={this.handleSubmit}
            />
        );
    }
}


// Here we wrap this MessageFormContainer with a Connect container,
// creating the MessageForm Container:
export default connect<null, DispatchFromProps>(
    null,
    mapDispatchToProps
)( withRouter( MessageFormContainer ) );
