/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as testRenderer from 'react-test-renderer';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { Provider } from "react-redux";
import store from "../../redux/store/index";
import { AppProps, App } from './';


// Basic FeedbackItem Test:
it
(
    'FeedbackItem renders correctly',
    () =>
    {
        let appItem: AppProps =
        {
            title: "FairyGodBoss"
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <App />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();

    /*  THIS DOES NOT WORK >:-(
        let tree = testRenderer.create
        (
            <Provider store={store}>
                <App />
            </Provider>
        ).toJSON();

        expect( tree ).toMatchSnapshot();
    */
    }
);
