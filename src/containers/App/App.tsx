/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { Switch, Route, HashRouter } from "react-router-dom";
import MessageBoard from "../MessageBoard";
import Message from "../Message";
import MessageForm from "../MessageForm";
import { ErrorHandler } from "../../components/ErrorHandler";


// DEFINES
export interface AppProps { title?: string; }


/**
 * Application, or root, component
 *
 * @param { AppProps } props
 *
 * @since 0.1.0
 */
export const App: React.SFC<AppProps> = ( props ) =>
{
    // Here we render the component:
    return (
        <HashRouter>
            <div className={styles.messageBoardContainerInner}>
                <div className={"row " + styles.contentBlock}>
                    <div className={"col-md-12 "  + styles.contentBlockInner}>
                        <Switch>
                            <Route exact path="/" component={MessageBoard}/>
                            <Route path="/view-post/:postid" component={Message}/>
                            <Route path="/create-post" component={MessageForm}/>
                            <Route component={ErrorHandler} />
                        </Switch>
                    </div>
                </div>
            </div>
        </HashRouter>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
App.displayName = "App";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 */


 /**
  * @var { any } defaultProps Set the defaultProps if you want
  */
App.defaultProps = {
    title: "FairyGodBoss"
}
