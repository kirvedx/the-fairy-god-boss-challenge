/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { MessageItemType } from "../../library";
import { MessageItem } from "../../components/MessageItem";


// DEFINES
export type MessageContainerProps = {
    posts: Array<MessageItemType>;
};

interface StateFromProps {
    posts: Array<MessageItemType>
}

const mapStateToProps = ( state: any ) =>
{
    return { posts: state.posts };
};


/**
 * Message container component
 *
 * @since 0.1.0
 */
export class MessageContainer extends React.Component<MessageContainerProps, any>
{
    /**
     * @var { string } displayName Always set the display name
     *
     * @since 0.1.0
     */
    displayName = "MessageContainer";


    /**
     * We'd set propTypes here if this wasn't Typescript.
     * We'd set default  props here if we wanted them.
     */


    /**
     * Gets the post Id from the url parameter
     *
     * @param { void }
     *
     * @return { number } Returns the post Id
     *
     * @since 0.1.0
     */
    getPostId(): number|boolean
    {
        // Get the postId from the url parameter configured with ReactRouter, and
        // then convert it to a number:
        let { postid } = ( this.props as any ).match.params,
        postId   = +postid;

        if( !postId || postId === 0 )
        {
            return false;
        }

        console.log( "postId: " + postId );

        return postId;
    }


    /**
     * Fetches the specific post we want to display from the
     * collection of posts currently stored in the store
     *
     * @param { void }
     *
     * @return { MessageItemType } Returns the post, by Id, specific as a url parameter
     *
     * @since 0.1.0
     */
    getPost(): MessageItemType|boolean
    {
        // Get the post Id specified in the url:
        let postId = this.getPostId();

        // Next, assign the posts from the Redux store to a variable for manipulation:
        let { posts } = this.props;

        // Next, filter the posts for the post with the id that we want:
        let post: any = posts.filter( post => post.id === postId );

        // If we have a new array with a length > 0, or specifically == 1:
        if( post && post.length == 1 )
        {
            // Snatch the post that was matched:
            post = post[0];
        }
        else
        {
            if( !post || !post.length )
            {
                return false;
            }
        }

        // In the Wire-frame provided for the challenge, there is no '@' symbol used
        // in the timestamp for this view:
        post.created = post.created.replace( '@ ', '' );

        return post;
    }


    /**
     * Container components pass rendering responsibilities off to
     * presentational components
     *
     * @param { void }
     *
     * @since 0.1.0
     */
   render()
    {
        if( !this.getPostId() || !this.getPost() )
        {
            return (
                <Redirect to="/error" />
            );
        }
        else
        {
            return (
                <MessageItem
                id={(this.getPostId() as number )}
                post={( this.getPost() as MessageItemType )}
                />
            );
        }
    }
}


// Here we wrap our MessageContainer with a Connect container,
// creating the Message Container:
export default connect<StateFromProps>(
    mapStateToProps
)( MessageContainer );
