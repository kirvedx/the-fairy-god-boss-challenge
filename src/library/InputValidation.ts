/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


 // INCLUDES


 // DEFINES
export type InputValidationType = {
    valid: boolean,
    exists: boolean;
    required: boolean;
    label: string;
    input: string;
    validation: ValidationType;
};

export type ValidationType = {
    class: string;
    message: string;
};
