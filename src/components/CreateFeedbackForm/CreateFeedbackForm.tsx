/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { InputValidationType } from "../../library";


// DEFINES
export type CreateFeedbackFormProps = {
    author: string;
    content: string;
    authorValidation: InputValidationType;
    contentValidation: InputValidationType;
    submitState: boolean;
    handleAuthorChange: React.FormEventHandler;
    handleReplyContentChange: React.FormEventHandler;
    handleSubmit: React.FormEventHandler;
};


/**
 * Create Feedback Form presentational omponent
 *
 * @param { CreateFeedbackFormProps } props
 *
 * @since 0.1.0
 */
export const CreateFeedbackForm: React.SFC<CreateFeedbackFormProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <div className={styles.createPostForm}>
            <form onSubmit={props.handleSubmit}>

                <div className={styles.formInputFields}>
                    <div className="form-group">
                        <label htmlFor="content" className={props.contentValidation.label}>Reply Message</label>
                        <textarea className={"form-control " + props.contentValidation.input} id="content" rows={6} onChange={props.handleReplyContentChange}>{props.content}</textarea>
                        <div className={"input-validation form-control-feedback " + props.contentValidation.validation.class} data-validation="input" data-for="content">
                            {props.contentValidation.validation.message}
                        </div>
                    </div>

                    <div className="form-group">
                        <label htmlFor="author"  className={props.authorValidation.label}>User</label>
                        <input
                            type="text"
                            className={"form-control " + props.authorValidation.input}
                            id="author"
                            placeholder="Your name"
                            value={props.author}
                            onChange={props.handleAuthorChange}
                        />
                        <div className={"input-validation form-control-feedback " + props.authorValidation.validation.class} data-validation="input" data-for="content">
                            {props.authorValidation.validation.message}
                        </div>
                    </div>
                </div>

                <button type="submit" className={"btn btn-outline-primary " + styles.createPostButton} disabled={props.submitState}>Create Post</button>
            </form>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
CreateFeedbackForm.displayName = "CreateFeedbackForm";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
