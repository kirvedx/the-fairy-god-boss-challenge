/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { NavLink } from "react-router-dom";


// DEFINES
export type MessageBoardItemProps = {
    id: number;
    title: string;
    content: string;
    excerpt: string;
    author: string;
    created: string;
    updated: string;
    commentCount: number;
};


/**
 * Message Board Item presentational component
 *
 * @param { MessageBoardItemProps } props
 *
 * @since 0.1.0
 */
export const MessageBoardItem: React.SFC<MessageBoardItemProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <li className={"list-group-item " + styles.messageItem} key={props.id} data-post-id={props.id}>
            <div className={styles.postPreviewContainer}>
                <div className={styles.postPreviewContainerTitle}>
                    <h5><NavLink to={"/view-post/" + props.id}>{props.title}</NavLink></h5>
                </div>
                <div className={styles.postPreviewContainerExcerpt}>
                    <p><NavLink to={"/view-post/" + props.id}>{props.excerpt}</NavLink></p>
                </div>
            </div>
            <div className={styles.postDetailsContainer + "  clearfix"}>
                <div className={styles.postDetailsContainerLeft}>
                    <div className={styles.postDetailsContainerLeftAuthor}>
                        By: <span className="field-highlight">{props.author}</span>
                    </div>
                    <div className={styles.postDetailsContainerLeftCommentCount}>
                        {props.commentCount} <span className={styles.fieldHighlight}>Comments</span>
                    </div>
                </div>
                <div className={styles.postDetailsContainerRight}>
                    <div className={styles.postDetailsContainerRightLastUpdateTimestamp}>
                        Last Update: <span className={styles.fieldHighlight}>{props.updated}</span>
                    </div>
                </div>
            </div>
        </li>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
MessageBoardItem.displayName = "MessageBoardItem";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
