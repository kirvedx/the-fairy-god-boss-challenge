/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { MessageBoardItemProps, MessageBoardItem } from './';


// Basic MessageBoardItem Test:
it
(
    'MessageBoardItem renders correctly',
    () =>
    {
        let messageBoardItem: MessageBoardItemProps =
        {
            id: 1,
            title: "Test Post",
            author: "Richard B Winters",
            excerpt: "",
            content: "This is a test post for testing purposes...DUH!",
            created: "12/06/18 2:28 PM",
            updated: "12/06/18 2:29 PM",
            commentCount: 2
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <MessageBoardItem
             id={messageBoardItem.id}
             title={messageBoardItem.title}
             author={messageBoardItem.author}
             excerpt={messageBoardItem.excerpt}
             content={messageBoardItem.content}
             created={messageBoardItem.created}
             updated={messageBoardItem.updated}
             commentCount={messageBoardItem.commentCount}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
