/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { NavLink } from "react-router-dom";


// DEFINES
export type ErrorHandlerProps = {
};


/**
 * MessageBoardList presentational component
 *
 * @param { ErrorHandlerProps } props
 *
 * @since 0.1.0
 */
export const ErrorHandler: React.SFC<ErrorHandlerProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <div className={styles.errorHandlerContainer}>

            <div className={"row " + styles.focusHeader}>
                <div className="col-md-12">
                    <h1>The <span className={styles.titleCapture}>FAIRYGODBOSS</span> Message Board</h1>
                </div>
            </div>

            <div className={styles.focusContent}>
                <div className={styles.errorWrap + " " + styles.errorMessage}>
                    <h1>Haxxed!?</h1>
                    <small>Naw, you wouldn't do that right? :)</small>
                    <p>You must have landed here by mistake! Click <NavLink to="/">HERE</NavLink> to be returned to the main page.</p>
                </div>
            </div>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
ErrorHandler.displayName = "ErrorHandler";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
