export const createPostContainer: string;
export const focusHeader: string;
export const headerWrap: string;
export const titleCapture: string;
export const createPostForm: string;
export const createPostButton: string;
export const cancelPostButton: string;
export const purpleDivider: string;
