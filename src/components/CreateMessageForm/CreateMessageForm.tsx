/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { NavLink } from "react-router-dom";
import { InputValidationType } from "../../library";


// DEFINES
export type CreateMessageFormProps = {
    title: string;
    author: string;
    content: string;
    titleValidation: InputValidationType;
    contentValidation: InputValidationType;
    authorValidation: InputValidationType;
    submitState: boolean;
    handleTitleChange: React.FormEventHandler;
    handleAuthorChange: React.FormEventHandler;
    handlePostContentChange: React.FormEventHandler;
    handleSubmit: React.FormEventHandler;
};


/**
 * Create Message Form presentational component
 *
 * @param { CreateMessageFormProps } props
 *
 * @since 0.1.0
 */
export const CreateMessageForm: React.SFC<CreateMessageFormProps> = ( props ) =>
{
    // Here we render the component:
    return(
        <div className={styles.createPostContainer}>

            <div className={"row " + styles.focusHeader}>
                <div className={"col-md-12 " + styles.headerWrap}>
                    <h1><span className={styles.titleCapture}>Create</span> a new post</h1>
                </div>
            </div>

            <div className={styles.createPostForm}>
                <form onSubmit={props.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="title" className={props.titleValidation.label}>Post Title</label>
                        <input
                            type="text"
                            className={"form-control " + props.titleValidation.input}
                            id="title"
                            placeholder="Post Title"
                            value={props.title}
                            onChange={props.handleTitleChange}
                        />
                        <div className={"input-validation form-control-feedback " + props.titleValidation.validation.class} data-validation="input">
                            {props.titleValidation.validation.message}
                        </div>
                    </div>

                    <hr className={styles.purpleDivider}/>

                    <div className="form-group">
                        <label htmlFor="content" className={props.contentValidation.label}>Message</label>
                        <textarea className={"form-control " + props.contentValidation.input} id="content" rows={6} onChange={props.handlePostContentChange}>{props.content}</textarea>
                        <div className={"input-validation form-control-feedback " + props.contentValidation.validation.class} data-validation="input" data-for="content">
                            {props.contentValidation.validation.message}
                        </div>
                    </div>

                    <hr className={styles.purpleDivider} />

                    <div className="form-group">
                        <label htmlFor="author"  className={props.authorValidation.label}>User</label>
                        <input
                         type="text"
                         className={"form-control " + props.authorValidation.input}
                         id="author"
                         placeholder="Your name"
                         value={props.author}
                         onChange={props.handleAuthorChange}
                        />
                        <div className={"input-validation form-control-feedback " + props.authorValidation.validation.class} data-validation="input" data-for="content">
                            {props.authorValidation.validation.message}
                        </div>
                    </div>

                    <button type="submit" className={"btn btn-outline-primary " + styles.createPostButton}  disabled={props.submitState}>Create Post</button>

                    <NavLink to="/">
                        <button type="button" className={"btn btn-outline-warning " + styles.cancelPostButton}>Cancel</button>
                    </NavLink>
                </form>
            </div>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
CreateMessageForm.displayName = "CreateMessageForm";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
