/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { NavLink } from "react-router-dom";
import { MessageItemType } from "../../library";
import { MessageBoardItem } from "../MessageBoardItem";


// DEFINES
export type MessageBoardListProps = {
    posts: MessageItemType[];
};


/**
 * MessageBoardList presentational component
 *
 * @param { MessageBoardListProps } props
 *
 * @since 0.1.0
 */
export const MessageBoardList: React.SFC<MessageBoardListProps> = ( props ) =>
{
    // Prepare the message board items for display:
    let classes: string = styles.messageList,
        messages: any = props.posts.map(
            ( post: any ) => (
                <MessageBoardItem
                id={post.id}
                title={post.title}
                author={post.author}
                content={post.content}
                excerpt={post.excerpt}
                created={post.created}
                updated={post.updated}
                commentCount={post.comments.length}
                />
            )
        );

    // Set a default in case no posts exist yet:
    if( !props.posts || !props.posts.length )
    {
        messages = "There are currently no posts...add one";
        classes += " " + styles.noMessages;
    }

    // Here we render the component:
    return(
        <div className={styles.messageListContainer}>

            <div className={"row " + styles.focusHeader}>
                <div className="col-md-12">
                    <h1>The <span className={styles.titleCapture}>FAIRYGODBOSS</span> Message Board</h1>
                </div>
            </div>

            <div className={styles.focusContent}>
                <div className={classes}>
                    {messages}
                </div>
            </div>

            <NavLink to="/create-post">
                <button type="button" className={"btn btn-outline-primary " + styles.createPostButton}>Create New Post</button>
            </NavLink>
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
MessageBoardList.displayName = "MessageBoardList";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
