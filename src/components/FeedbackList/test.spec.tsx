/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ShallowRenderer from 'react-test-renderer/shallow';

import { FeedbackListProps, FeedbackList } from './';


// Basic FeedbackList Test (empty):
it
(
    'FeedbackList renders correctly with an empty comments array',
    () =>
    {
        let feedbackList: FeedbackListProps =
        {
            post:
            {
                id: 1,
                title: "Test Post",
                author: "Richard B Winters",
                excerpt: "",
                content: "This is a test post for testing purposes...DUH!",
                created: "12/06/18 2:28 PM",
                updated: "12/06/18 2:29 PM",
                comments: []
            }
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render
        (
            <FeedbackList
              post={feedbackList.post}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);


// Basic FeedbackList Test (2 comments):
it
(
    'FeedbackList renders correctly with a single post in the  comments array',
    () =>
    {
        let feedbackList: FeedbackListProps =
        {
            post: {
                id: 1,
                title: "Test Post",
                author: "Richard B Winters",
                excerpt: "",
                content: "This is a test post for testing purposes...DUH!",
                created: "12/06/18 2:28 PM",
                updated: "12/06/18 2:29 PM",
                comments: [
                    { id: 0, parentId: 0,  author: "Richard", content: "This is a funny post!", created: "09/06/18 @ 03:22 PM" },
                    { id: 1, parentId: 0, author: "Paul", content: "Great post!", created: "09/06/18 @ 03:28 PM" }
                ]
            }
        };

        let renderer = ShallowRenderer.createRenderer();

        renderer.render(
            <FeedbackList
              post={feedbackList.post}
            />
        );

        expect( renderer.getRenderOutput() ).toMatchSnapshot();
    }
);
