/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { MessageItemType } from "../../library";
import { FeedbackItem } from "../FeedbackItem";



// DEFINES
export type FeedbackListProps = {
    post: MessageItemType;
};


/**
 * FeedbackList presentational component
 *
 * @param { FeedbackListProps } props
 *
 * @since 0.1.0
 */
export const FeedbackList: React.SFC<FeedbackListProps> = ( props ) =>
{
    // Prep a comments list (if any comments exist):
    let classes: string = styles.commentList,
        responses: any = ( props.post.comments && props.post.comments.length ) ? props.post.comments.map(
            ( comment: any ) => (
                <FeedbackItem
                 id={comment.id}
                 author={comment.author}
                 content={comment.content}
                />
            )
        ): '';

    // Set a default in case no comments exist yet:
    if( !props.post.comments || !props.post.comments.length )
    {
        responses = "There are currently no comments...add one!";
        classes += " " + styles.noComments;
    }

    // Here we render the component:
    return(
        <div className={classes}>
            {responses}
        </div>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
FeedbackList.displayName = "FeedbackList";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
