export const commentItem: string;
export const commentBodyContainer: string;
export const commentHeaderContainer: string;
export const postHeaderContainer: string;
export const postHeaderContainerDetails: string;
export const fieldHighlight: string;
export const commentBodyContent: string;
