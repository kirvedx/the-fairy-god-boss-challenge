/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";


// DEFINES
export type FeedbackItemProps = {
    id: number,
    author: string,
    content: string
}


/**
 * Feedback Item presentational component
 *
 * @param { FeedbackItemProps } props
 *
 * @since 0.1.0
 */
export const FeedbackItem: React.SFC<FeedbackItemProps> = ( props ) =>
{
    // Here we render the component:
    return(

        <li className={"list-group-item " + styles.commentItem} key={props.id} data-post-id={props.id}>
            <div className={styles.commentHeaderContainer}>
                <div className={styles.postHeaderContainerDetails}>
                    By: <span className={styles.fieldHighlight}>{props.author}</span>
                </div>
            </div>
            <div className={styles.commentBodyContainer}>
                <div className={styles.commentBodyContent}>
                    <p>{props.content}</p>
                </div>
            </div>
        </li>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
FeedbackItem.displayName = "FeedbackItem";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted them.
 */
