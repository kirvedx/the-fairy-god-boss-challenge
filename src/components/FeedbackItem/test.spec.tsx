/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as testRenderer from 'react-test-renderer';

import { FeedbackItemType } from "../../library";
import { FeedbackItem } from './';


// Basic FeedbackItem Test:
it
(
    'FeedbackItem renders correctly',
    () =>
    {
        let feedbackItem: FeedbackItemType =
        {
            id: 1,
            parentId: 1,
            author: "Richard B Winters",
            content: "Great post my man!",
            created: "09/05/18 @ 03:22 PM"
        };

        let tree = testRenderer.create
        (
            <FeedbackItem
             id={feedbackItem.id}
             author={feedbackItem.author}
             content={feedbackItem.content}
            />
        ).toJSON();

        expect( tree ).toMatchSnapshot();
    }
);
