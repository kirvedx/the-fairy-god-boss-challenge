/*------------------------------------------------------------------------------
 * @package:   fairy-god-boss-challenge
 * @author:    Richard B Winters
 * @copyright: 2018 Richard B Winters.
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import * as styles from "./styles.scss";


// EXPORTS
export { styles };
export {
    FeedbackItem,
    FeedbackItemProps
} from "./FeedbackItem";
